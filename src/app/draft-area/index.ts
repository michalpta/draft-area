import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DraftAreaComponent } from './draft-area.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    DraftAreaComponent
  ],
  exports: [
    DraftAreaComponent
  ]
})
export class DraftAreaModule { }
