import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';

import { Observable, Subject, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'draft-area',
  templateUrl: './draft-area.component.html',
  styleUrls: ['./draft-area.component.scss']
})
export class DraftAreaComponent implements OnInit, OnDestroy {

  @Input()
  value = '';
  @Output()
  valueChange: EventEmitter<string> = new EventEmitter();

  @Input()
  key = 'comment';

  private get prefixedKey() {
    return `ng-draft-area|${this.key}`;
  }

  @Input()
  label = 'Comment';

  @Input()
  rows = 10;

  private debounceTime = 250;

  draft: string = null;
  get hasDraft() {
    return this.draft != null;
  };

  private keyup$: Subject<any> = new Subject();
  private draftRestore$: Subject<any> = new Subject();

  private newValue$: Observable<string>;

  private sub: Subscription;

  constructor() { }

  ngOnInit() {
    const draft = localStorage.getItem(this.prefixedKey);
    if (this.draft !== this.value) {
        this.draft = draft;
    }

    this.newValue$ = Observable.merge(
      this.keyup$
        .map(() => this.value)
        .distinctUntilChanged(),
      this.draftRestore$
        .map(() => this.draft)
        .do(() => this.draft = null)
    );

    this.sub = this.newValue$
      .do(v => this.valueChange.emit(v))
      .debounceTime(this.debounceTime)
      .distinctUntilChanged()
      .subscribe(v => localStorage.setItem(this.prefixedKey, v));
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  emitKeyUp() {
    this.keyup$.next();
  }
  emitDraftRestore() {
    this.draftRestore$.next();
  }
  discardDraft() {
    this.draft = null;
    localStorage.removeItem(this.prefixedKey);
  }
}
