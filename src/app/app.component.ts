import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  value = 'This is a sketch which is already saved in the database. \nNow change it and refresh without saving.'
  refresh = () => location.reload();
}
